package battleship;
import battleship.util.Position;

public class SecondBattleShipMain {
    public static void main(String[] args) {
        Sea sea = new Sea(10, 10);
        
        // add first ship
        Ship robertShip = new Ship(3);
        sea.addShip(robertShip, new Position(4,5));
        sea.addShip(robertShip, new Position(5,5));
        sea.addShip(robertShip, new Position(6,5));
        
        // add second ship
        Ship philibertShip = new Ship(2);
        sea.addShip(philibertShip, new Position(3,8));
        sea.addShip(philibertShip, new Position(3,9));
        
        // display initial state
        sea.display(true);
        System.out.println("---------------------------");

        // running battleship game
        Game battle1 = new Game(sea);
        battle1.play();
    }
    
}
