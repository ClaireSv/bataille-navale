package battleship;
import battleship.util.Position;

public class ThirdBattleShipMain {
    public static void main(String[] args) {
        Sea board = new Sea(10, 10);
        Ship ship0Size2 = new Ship(2);
        Ship ship1Size2 = new Ship(2);
        Ship ship0Size3 = new Ship(3);
        Ship ship1Size3 = new Ship(3);
        Ship shipSize4  = new Ship(4);
        Ship shipSize5  = new Ship(5);

        board.addWholeShip(ship0Size2, new Position(7, 2), 1, 0);
        board.addWholeShip(ship1Size2, new Position(1, 2), 0, 1);

        board.addWholeShip(ship0Size3, new Position(4, 0), 1, 0);
        board.addWholeShip(ship1Size3, new Position(0, 4), 1, 0);

        board.addWholeShip(shipSize4, new Position(6, 5), 0, 1);
        board.addWholeShip(shipSize5, new Position(4, 4), 1, 0);

        board.display(true);
        System.out.println("---------------------------");

        Game game = new Game(board);
        game.play();
        System.out.println("You won !");
    }
}
