package battleship;

/** Answers possible for the state of the cell targeted */
public enum Answer {
    /** Shot missed */
    MISSED,
    /** Shot succeded to hit a ship */
    HIT,
    /** Shot succeded to hit a ship causing it to sunk */
    SUNK
}
