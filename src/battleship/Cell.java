package battleship;

/** Cell's class */
public class Cell {

    /** The ship on this cell if it exists */
    private Ship ship;
    /** If the cell has been shot */
    private boolean shot;

    /**
     * Cell's constructor
     */
    public Cell(){
        this.ship = null;
        this.shot = false;
    }
    
    /**
     * Return true if the Cell is empty ie there's no ship on it, else false
     * @return true if the Cell is empty, else false
     */
    public boolean isEmpty(){
        return this.ship == null;
    }

    /**
     * Getter for the ship in this cell, else return null
     * @return the ship in this cell if there is one, else return null
     */
    public Ship getShip(){
        return this.ship;
    }

    /**
     * Setter to place a new ship in this cell
     * @param newShip the new ship to place in this cell
     */
    public void setShip(Ship newShip){
        this.ship = newShip;
    }

    /**
     * Return the answer of the cell, if there is a ship: sunk or hit, and missed if there's no ship.
     * @return the answer of the cell
     */
    public Answer shot(){
        if (this.ship == null || this.shot){
            this.shot = true;
            return Answer.MISSED;
        }
        else this.ship.beenHitting();
        
        this.shot = true;
        if (this.ship.hasBeenSunk()){
            return Answer.SUNK;
        }
        else {
            return Answer.HIT;
        }
    }

    /**
     * Return true if the cell has been shot, else false
     * @return Return true if the cell has been shot, else false
     */
    public boolean hasBeenShot(){
        return this.shot;
    }

    /**
     * Display method: 
     * For the defender : 
     * Return '~' if this cell is emmpty, 'B' if there is a ship on and it has not been shot, '*' if the ship on this cell has been shot.
     * For the assayant : 
     * Return '.' if this cell has not been shot, '~' if it has been shot but there was nothing on it, '*' if this has been shot and there was a ship on it.
     * @param defender true if the method is call by a defender, else false.
     * @return For the defender : 
     * Return '~' if this cell is emmpty, 'B' if there is a ship on and it has not been shot, '*' if the ship on this cell has been shot.
     * For the assayant : 
     * Return '.' if this cell has not been shot, '~' if it has been shot but there was nothing on it, '*' if this has been shot and there was a ship on it.
     */
    public char toCharacter(boolean defender)
    {
        if(defender) {
            if(this.shot) return '*';
            if(!this.isEmpty())    return 'B';
            return '~';
        } 
        else {
            if(!this.shot) return '.';
            if(this.isEmpty()) return '~';
            return '*';
        }
    }
}
