package battleship;

import battleship.util.Position;

public class FirstBattleShipMain {
    public static void main(String[] args) {
        Sea sea = new Sea(10, 10);
        // add first boat
        Ship robertShip = new Ship(3);
        sea.addShip(robertShip, new Position(4,5));
        // add second boat
        Ship philibertShip = new Ship(2);
        sea.addShip(philibertShip, new Position(3,9));

        // display
        System.out.println("Point of view: Own sea");
        sea.display(true);
        System.out.println("-----------------\nPoint of view: opponent sea");
        sea.display(false);
        System.out.println("-----------------");

        // shoot & display
        sea.shoot(new Position(4,5));
        System.out.println("Point of view: Own sea");
        sea.display(true);
        System.out.println("-----------------\nPoint of view: opponent sea");
        sea.display(false);
        System.out.println("-----------------");

    }
    
}
