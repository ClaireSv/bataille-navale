package battleship;
import battleship.util.Position;
import io.Input;

/**
 * Game class
 */
public class Game {
   /** the sea of this game */
   private Sea sea;

   /**
    * Create a game
    * @param sea The sea where the game is
    */
   public Game(Sea sea) {
       this.sea = sea;
   }

    /**
     * Play a game of battleship
     */
    public void play(){
        while (this.sea.getRemainingLifePoints() > 0){
            sea.display(false);
            Position position = inputPosition();
            if (position.getX() == -1 || position.getY() == -1){
                continue;
            }
            Answer answer = sea.shoot(position);
            System.out.println(answer);
        }
        System.out.println("End");
    }

    /**
     * Return the sea of this game
     * @return the sea of this game
     */
    public Sea getSea() {
        return this.sea;
    }

    /**
     * Get a position input from user and return it
     * @return the position input by the user
     */
    private Position inputPosition() {
        System.out.println("Where do you want to shoot ? (XY)");
        String input = Input.readString();
        char x = input.charAt(0);
        char y = input.charAt(1);
        
        int posX = -1, posY = -1;
        // parse x
        if('A' <= x && x <= 'Z')
            posX = x - 'A';
        else if('a' <= x && x <= 'z')
            posX = x - 'a';
        else if('0' <= x && x <= '9')
            posX = x - '0';

        // parse y
        if('0' <= y && y <= '9')
            posY = y - '0';
        

        return new Position(posX, posY);
    }
}
