package battleship;

import battleship.util.Position;

/**
 * Represent a board
 */
public class Sea {
    /** Game grid */
    private Cell[][] sea;

    /** Total life points */
    private int fullLifePoints;

    /**
     * Create a sea with empty cells
     * @param sizeX Size X of the grid
     * @param sizeY Size Y of the grid
     */
    public Sea(int sizeX, int sizeY) {
        this.sea = new Cell[sizeY][sizeX];
        for(int y = 0; y <  sizeY; ++y)
            for(int x = 0; x <  sizeX; ++x)
                this.sea[y][x] = new Cell();
        fullLifePoints = 0;
    }

    /**
     * Shoot in a specified cell in the sea
     * @param pos position where to shoot
     * @return the answer of this shot
     */
    public Answer shoot(Position pos) {
        Answer ans = this.sea[pos.getY()][pos.getX()].shot();
        if(ans == Answer.HIT || ans == Answer.SUNK)
            fullLifePoints--;
        return ans;
    }

    /**
     * Return the remaining life points of this sea
     * @return the remaining life points of this sea
     */
    public int getRemainingLifePoints() {
        return this.fullLifePoints;
    }

    //////////////////////////////////////////////
    //// ADD SHIP

    /**
     * Add a ship to a grid cell
     * @param ship ship to add
     * @param pos position in the grid
     * @exception IndexOutOfBoundsException the position isn't in the grid
     */
    public void addShip(Ship ship, Position pos) throws IndexOutOfBoundsException {
        this.sea[pos.getY()][pos.getX()].setShip(ship);
        this.fullLifePoints += 1;
    }

    /** add the ship b to this game’s sea. Ship is added with dir (dx, dy) from position p.
     * The number of cells is determined by the ship life points.
     * @param ship the ship to add
     * @param pos the position of the first cell occupied by the ship
     * @param dx the x direction of the ship
     * @param dy the y direction of the ship
     * @throws IllegalStateException if the ship b can not be placed on the sea
     * (ship goes outside of the sea or some cell is not empty)
     */
    public void addWholeShip(Ship ship, Position pos, int dx, int dy) throws IllegalStateException {
        this.testIfShipCanBePlaced(ship, pos, dx, dy);

        for(int l = 0; l < ship.getLifePoints(); ++l) {
            int x = l * dx;
            int y = l * dy;
            this.sea[y + pos.getY()][x + pos.getX()].setShip(ship);
            this.fullLifePoints += 1;
        }
    }

    /**
     * Test if we can place the ship in the direction wanted from the position of the ship
     * @param ship the ship to add
     * @param pos the position of the first cell occupied by the ship
     * @param dx the x direction of the ship
     * @param dy the y direction of the ship
     * @throws IllegalStateException if the ship b can not be placed on the sea
     * (ship goes outside of the sea or some cell is not empty)
     */
    public void testIfShipCanBePlaced(Ship ship, Position pos, int dx, int dy) throws IllegalStateException {
        // check validity of dx and dy
        if(!(dx == 1 && dy == 0) && !(dx == 0 && dy == 1))
            throw new IllegalStateException();

        // check empty cells or outside bounds
        try {
            for(int l = 0; l < ship.getLifePoints(); ++l) {
                int x = l * dx;
                int y = l * dy;
                if(!this.sea[y + pos.getY()][x + pos.getX()].isEmpty()) {
                    throw new IllegalStateException();
                }
            }
        }
        catch(Exception e) {
            throw new IllegalStateException();
        }
    }

    
    /** add the ship b to this game’s sea. Ship is added vertically down from position p.
    * The number of cells is determined by the ship life points.
    * @param shipToPlace the ship to add
    * @param position the position of the first (top) cell occupied by the ship
    * @throws IllegalStateException if the ship b can not be placed on the sea
    * (ship goes outside of the sea or some cell is not empty)
    */ 
    public void addShipVertically(Ship shipToPlace, Position position) throws IllegalStateException {
        this.testIfShipCanBePlacedVertically(shipToPlace, position);
        // place the ship
        for(int y = 0; y < shipToPlace.getLifePoints(); y++) {
            this.sea[y + position.getY()][position.getX()].setShip(shipToPlace);
            this.fullLifePoints++;
        }
    }

    /**
     * Test if we can place the ship vertically from the top position of the ship
     * @param shipToPlace the ship to add
     * @param position the position of the first (top) cell occupied by the ship
     * @throws IllegalStateException if the ship b can not be placed on the sea
     * (ship goes outside of the sea or some cell is not empty)
     */
    private void testIfShipCanBePlacedVertically(Ship shipToPlace, Position position) throws IllegalStateException {
        // test if end of the ship is outside the sea
        if(position.getY() + shipToPlace.getLifePoints() >= this.sea.length)
            throw new IllegalStateException();
        
        // test if the cells where we want to place the boat are empty
        for(int y = 0; y < shipToPlace.getLifePoints(); y++) {
            if(!this.sea[y + position.getY()][position.getX()].isEmpty()) {
                throw new IllegalStateException();
            }
        }
    }

    /** add the ship b to this game’s sea. Ship is added horizontally right from position p.
    * The number of cells is determined by the ship life points.
    * @param shipToPlace the ship to add
    * @param position the position of the first (left) cell occupied by the ship
    * @throws IllegalStateException if the ship b can not be placed on the sea
    * (ship goes outside of the sea or some cell is not empty)
    */
    public void addShipHorizontally(Ship shipToPlace, Position position) throws IllegalStateException {
        this.testIfShipCanBePlacedHorizontally(shipToPlace, position);
        // place the ship
        for(int x = 0; x < shipToPlace.getLifePoints(); x++) {
            this.sea[position.getY()][x + position.getX()].setShip(shipToPlace);
            this.fullLifePoints++;
        }
    }

    /**
     * Test if we can place the ship Horizontally from the top position of the ship
     * @param shipToPlace the ship to add
     * @param position the position of the first (top) cell occupied by the ship
     * @throws IllegalStateException if the ship b can not be placed on the sea
     * (ship goes outside of the sea or some cell is not empty)
     */
    private void testIfShipCanBePlacedHorizontally(Ship shipToPlace, Position position) throws IllegalStateException {
        // test if end of the ship is outside the sea
        if(position.getX() + shipToPlace.getLifePoints() >= this.sea[0].length)
            throw new IllegalStateException();
        
        // test if the cells where we want to place the boat are empty
        for(int x = 0; x < shipToPlace.getLifePoints(); x++) {
            if(!this.sea[position.getY()][x + position.getX()].isEmpty()) {
                throw new IllegalStateException();
            }
        }
    }

    //////////////////////////////////////////////
    /// DISPLAY

    /** display the game board line by line and cell by cell, on standard output,
    * the display is different for the defender or the attacker, according to parameter
    * @param defender true if display is for defender, false otherwise
    */
    public void display(boolean defender) {
        this.displayCoordinates();
        // display lines
        for(int y = 0; y < this.sea.length; y++) {
            // each cell + left number
            this.displayLongLine(this.sea[0].length+1, 4, '┼','┤');
            this.displaySeaLine(y, defender);
        }
        this.displayLongLine(this.sea[0].length+1, 4, '┴','┘');
    }

    /**
     * Display the top coordinates of the grid
     */
    private void displayCoordinates() {
        // display x coordinates (letters)
        String res = "    │";
        for(int x = 0; x < this.sea[0].length; x++) {
            res += " " + (char)('A' + x) + " │";
        }
        System.out.println(res);
    }

    /**
     * Display a line of the sea
     * @param lineIndex index of line in the sea
     * @param defender true if display is for defender, false otherwise
     */
    private void displaySeaLine(int lineIndex, boolean defender) {
        String res = String.format("%3d ", lineIndex);
        for(int x = 0; x < this.sea[lineIndex].length; x++) {
            res += "│ " + this.sea[lineIndex][x].toCharacter(defender) + " ";
        }
        res += '│';
        System.out.println(res);
    }

    /**
     * Display a long line of ─ interleaved with the separationChar and ending with the endChar
     * @param lineLength number of times the pattern must repeat in the line
     * @param patternSize number-1 of ─ in the pattern
     * @param separationChar char between each pattern
     * @param endChar char at the end of the line
     */
    private void displayLongLine(int lineLength, int patternSize, char separationChar, char endChar) {
        String line = "─";
        for(int i = 0; i < lineLength-1; ++i) {
            for(int j = 0; j < patternSize-1; ++j) {
                line += '─';
            }
            line += separationChar;
        }
        for(int j = 0; j < patternSize-1; ++j) {
            line += '─';
        }
        line += endChar;
        System.out.println(line);
    }
}
