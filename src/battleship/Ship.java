package battleship;

/** Ship class */ 
public class Ship {
    /** the nb of life points  */
    private int lifePoints;

    /**
     * Ship's constructor
     * @param length - Ship's length
     */
    public Ship(int length){
        this.lifePoints = length;
    }
    /**
     * Return true if the ship has been sunk, else false
     * @return true if the ship has been sunk, else false
     */
    public boolean hasBeenSunk(){
        return this.lifePoints == 0;
    }

    /**
     * Decrease the nb of life points by 1
     */
    public void beenHitting() {
        this.lifePoints -- ;
    }

    /**
     * Give the nb of life points
     * @return the nb of life points
     */
    public int getLifePoints(){
        return this.lifePoints;
    }

    /**
     * Return this ship's description
     * @return the description of the ship
     */
    public String toString(){
        return "This ship has " + this.lifePoints + " life points";
    }

}
