package battleship;

import static org.junit.Assert.*;

import org.junit.Test;


public class CellTest {
    @Test
    public void TestdecreaseLifePointsWhenHit() {
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        cell.setShip(ship);
        assertEquals(ship.getLifePoints(), 2);
        Answer answer = cell.shot();
        assertEquals(ship.getLifePoints(), 1);
        assertEquals(answer, Answer.HIT);
   }

    @Test
    public void TestTwoShots(){
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        cell.setShip(ship);
        cell.shot();
        Answer answer = cell.shot();
        assertEquals(ship.getLifePoints(), 1);
        assertEquals(answer, Answer.MISSED);
    }

    @Test
    public void TestTwoShotsReturnMissed(){
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        cell.setShip(ship);
        assertFalse(cell.hasBeenShot());
        cell.shot();
        Answer answer = cell.shot();
        assertTrue(cell.hasBeenShot());
        assertEquals(answer, Answer.MISSED);

    }

    @Test
    public void TestSunk(){
        Ship ship = new Ship(1);
        Cell cell = new Cell();
        cell.setShip(ship);
        assertFalse(ship.hasBeenSunk());
        Answer answer = cell.shot();
        assertTrue(ship.hasBeenSunk());
        assertEquals(answer, Answer.SUNK);
    }

    @Test
    public void TestToCharacterForDefenderTrue(){
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        assertEquals('~', cell.toCharacter(true));
        cell.setShip(ship);
        assertEquals('B', cell.toCharacter(true));
        cell.shot();
        assertEquals('*', cell.toCharacter(true));
    }

    @Test
    public void TestToCharacterForDefenderFalse(){
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        assertEquals('.', cell.toCharacter(false));
        cell.shot();
        assertEquals('~', cell.toCharacter(false));
        
        cell = new Cell();
        cell.setShip(ship);
        assertEquals('.', cell.toCharacter(false));
        cell.shot();
        assertEquals('*', cell.toCharacter(false));
    }



    // --------Pour permettre l'exécution des test---------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(tp8.test.goosegame.CellTest.class);
    }
}
