package battleship;
import battleship.util.Position;

import static org.junit.Assert.*;

import org.junit.Test;

public class SeaTest {

    // creation state test

    @Test
    public void TestValidCreationState() {
        Sea sea = new Sea(2,2);
        assertTrue(sea.getRemainingLifePoints() == 0);
        // No boat
        for(int x = 0; x < 2; ++x)
            for(int y = 0; y < 2; ++y)
                assertEquals(sea.shoot(new Position(x, y)), Answer.MISSED);
    }

    // add ship test

    @Test
    public void TestAddShip(){
        Ship ship = new Ship(1);
        Cell cell = new Cell();
        Sea sea = new Sea(4, 4);
        Position position = new Position(1,2);
        assertTrue(sea.getRemainingLifePoints() == 0);
        sea.addShip(ship, position);
        assertEquals(sea.getRemainingLifePoints(),ship.getLifePoints());

    }

    // shoot test

    @Test (expected = IndexOutOfBoundsException.class)
    public void ShootReturnExceptionWhenOutOfBounds(){
        Ship ship = new Ship(2);
        Cell cell = new Cell();
        Sea sea = new Sea(4, 4);
        Position position = new Position(5,2);
        sea.shoot(position);
    }

    @Test
    public void ShootReturnAnswerForThePosition(){
        Ship ship = new Ship(2);
        Sea sea = new Sea(4, 4);
        assertEquals(sea.shoot(new Position(0, 0)), Answer.MISSED);
        Position position = new Position(1,2);
        sea.addShip(ship, position);
        assertEquals(sea.shoot(position), Answer.HIT);
    }

    @Test
    public void TestCorrectLifePointsAfterHit() {
        Sea sea = new Sea(5, 5);
        Ship ship = new Ship(2);
        sea.addShip(ship, new Position(1, 1));
        sea.addShip(ship, new Position(2, 1));
        assertTrue(sea.getRemainingLifePoints() == 2);
        sea.shoot(new Position(1, 1));
        assertTrue(sea.getRemainingLifePoints() == 1);
        sea.shoot(new Position(2, 1));
        assertTrue(sea.getRemainingLifePoints() == 0);
    }

    // Add Whole ship tests
    
    @Test
    public void TestCanPlaceShipVertically() {
        Sea sea = new Sea(5, 5);
        Ship ship = new Ship(2);
        sea.addWholeShip(ship, new Position(0, 0), 0, 1);
        assertTrue(sea.getRemainingLifePoints() == ship.getLifePoints());
    }

    @Test (expected = IllegalStateException.class)
    public void TestCannotPlaceShipOutsideTheSea() {
        Sea sea = new Sea(5, 5);
        Ship ship = new Ship(2);
        sea.addWholeShip(ship, new Position(0, 4), 0, 1);
    }

    @Test (expected = IllegalStateException.class)
    public void TestCannotPlaceShipWhereThereIsAShip() {
        Sea sea = new Sea(5, 5);
        Ship ship1 = new Ship(2);
        Ship ship2 = new Ship(1);
        sea.addShip(ship2, new Position(1, 2));
        sea.addWholeShip(ship1, new Position(1, 1), 0, 1);
    }

    @Test
    public void TestCanPlaceShipHorizontally() {
        Sea sea = new Sea(5, 5);
        Ship ship = new Ship(2);
        sea.addWholeShip(ship, new Position(0, 0), 1, 0);
        assertTrue(sea.getRemainingLifePoints() == ship.getLifePoints());
    }

    @Test (expected = IllegalStateException.class)
    public void TestCannotPlaceShipInDiagonal() {
        Sea sea = new Sea(5, 5);
        Ship ship = new Ship(2);
        sea.addWholeShip(ship, new Position(0, 0), 1, 1);
    }

    // --------Pour permettre l'exécution des test---------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(battleship.SeaTest.class);
    }
    
}
