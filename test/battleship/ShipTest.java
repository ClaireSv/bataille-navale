package battleship;

import static org.junit.Assert.*;

import org.junit.Test;


public class ShipTest {
    @Test
    public void TestDecreaseLifePointsWhenShipBeenHitting() {
        Ship ship = new Ship(2);
        ship.beenHitting();
        assertTrue(ship.getLifePoints() == 1);
    }

   @Test
    public void TestLifePointsNullWhenSunk() {
        Ship ship = new Ship(1);
        ship.beenHitting();
        assertTrue(ship.getLifePoints() == 0);
        assertTrue(ship.hasBeenSunk());
    }

    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(battleship.ShipTest.class);
    }
}
