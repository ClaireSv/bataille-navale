# TP05 - BATAILLE NAVALE

Authors: Zanardo Lucas & Savel-Vioux Claire

## Objectifs

- Mise en pratique de coder nos propres classes, tests et documentation

## Ajouts

- Ajout de la documentation manquante dans les fichiers nécessaire (ou il y avait des warnings)
- Ajout d'un rendu plus propre pour display

## Usage

> Compilation: `javac src/*.java`
>
> FirstBattleShipMain: `java battleship.FirstBattleShipMain`
>
> SecondBattleShipMain: `java battleship.SecondBattleShipMain`
>
> ThirdBattleShipMain: `java battleship.ThirdBattleShipMain`

## Test

> Compilation Tests : `javac -cp test4poo.jar test/battleship/*.java`
>
> CellTest: `java -jar battleship.CellTest`
>
> ShipTest: `java -jar battleship.ShipTest`
>
> SeaTest: `java -jar battleship.SeaTest`

## Documentation

> Compilation : `javadoc -sourcepath src/ -d docs -subpackages battleship io`
>
> Ouvrir [docs/index.html](docs/index.html)